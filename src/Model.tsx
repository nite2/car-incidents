import {Datum} from 'plotly.js';
import {IEventData} from './plot';

export interface IAppState {
    data: IEventData[];
    filters: { [x: string]: any[] | Datum[]; };
    timeRange: Date[];
    colorBy: string;
    zoneCount: number;
}

export const plotStyle = {width: '100%', height: '100%'};
