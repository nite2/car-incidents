import {mean} from 'd3-array';
import {Dayjs, UnitType} from 'dayjs';
import {Layout, Margin, PlotData} from 'plotly.js';
import {Figure} from 'react-plotly.js';
import {
    cloneDeep,
    groupBy,
    map,
    countBy,
    values,
    keys,
} from 'lodash';

const mapboxAccessToken = 'pk.eyJ1Ijoibml0ZSIsImEiOiJjanlsaDd1aGcwN240M25xZDBteDJwNTQ5In0.OSGs-Sy-07GFXfxFf83kXw';

const bgcolor = '#303030';
const defaultLayout: Partial<Layout> = {
    autosize: true,
    barmode: 'relative',
    xaxis: {'title': ''},
    yaxis: {'title': ''},
    margin: {'l': 40, 'r': 10, 't': 40, 'b': 0},
    paper_bgcolor: bgcolor,
    plot_bgcolor: bgcolor,
    font: {
        color: 'white',
    },
};

export interface IEventData {
    lat: number;
    long: number;
    reason: string;
    action: string;
    zone: string;

    [key: string]: any;

    ts: Date;
}

export function mapbox(data: IEventData[], group = 'zone') {
    const traces = map(
        groupBy(data, d => d[group]),
        (group, key) => ({
            type: 'scattermapbox',
            lat: group.map(d => d.lat),
            lon: group.map(d => d.long),
            mode: 'markers',
            name: key,
            marker: {
                size: 6,
            },
            text: ['Montreal']
        }));

    const layout: Partial<Layout> = {
        ...cloneDeep(defaultLayout),
        hovermode: 'closest',
        showlegend: true,
        mapbox: {
            bearing: 10,
            center: {
                lat: mean(data.map(d => d.lat)),
                lon: mean(data.map(d => d.long)),
            },
            pitch: 60,
            zoom: 13.5,
            style: 'dark',
        },
    };
    const config = {
        mapboxAccessToken,
    };
    return {
        data: traces,
        layout,
        config,
    } as Figure;
}

/***
 * Round a date to nearest specified time, eg. roundDate(dayjs, 'minute', 30)
 *
 * @param date
 * @param offset
 * @param type
 */
export function roundDate(date: Dayjs,
                          type: UnitType,
                          offset: number) {
    date = date.clone();
    const roundedVal = Math.floor(date.get(type) / offset) * offset;
    return date.set(type, roundedVal)
        .millisecond(0)
        .second(0)
        .toISOString();
}

export function countBarPlot(data: IEventData[],
                             col: string,
                             l = 130,
                             selectedpoints: any[] = []): Figure {
    const counts = countBy(data, col);
    const margin = {
        l,
        r: 10,
        t: 40,
        b: 0,
    };
    return barPlot(
        values(counts),
        keys(counts),
        col,
        margin,
        'h',
        selectedpoints);
}

export function barPlot(x: any[],
                        y: any[],
                        col: string,
                        margin: Margin,
                        orientation: 'v' | 'h' = 'v',
                        selectedpoints: any[] = []) {
    const data: Partial<PlotData>[] = [{
        x: x,
        y: y,
        type: 'bar',
        orientation,
    }];
    const selectdirection = orientation === 'h' ? 'v' : 'h';

    const layout = {
        ...cloneDeep(defaultLayout),
        margin,
        yaxis: col === 'zone' ? {dtick: 1} : {},
        barmode: 'group',
        bargroupgap: 0.1,
        dragmode: 'select',
        selectdirection,
        selectedpoints,
        title: {
            x: 0.05,
            text: col,
        },
    } as Partial<Layout>;

    return {
        data,
        layout,
    };
}
