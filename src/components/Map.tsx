import React from 'react';
import Plot from 'react-plotly.js';
import {entries, every} from 'lodash';

import {mapbox} from '../plot';
import {IAppState, plotStyle} from '../Model';

export function MapPlot(props: IAppState) {
    const {
        data,
        filters,
        timeRange,
        colorBy,
    } = props;

    const filterPairs = entries(filters);
    const filteredData = data.filter(datum => {
            let result = true;
            if (filterPairs.length) {
                result = result && every(
                    filterPairs.map(([key, value]) => value.includes(datum[key]))
                );
            }
            if (timeRange.length) {
                result =  result && datum.ts >= timeRange[0] && datum.ts <= timeRange[1];
            }
            return result;
        }
    );

    const mapPlot = mapbox(filteredData, colorBy);
    return <Plot {...mapPlot}
                 divId='map'
                 useResizeHandler={true}
                 style={plotStyle}/>;
}
