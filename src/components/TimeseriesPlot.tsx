import dayjs from 'dayjs';
import React, {Component} from 'react';
import Plot from 'react-plotly.js';
import {isEqual, keys, values, groupBy} from 'lodash';

import {barPlot, IEventData, roundDate} from '../plot';
import {plotStyle} from '../Model';

interface IFilterProps {
    data: IEventData[];
    onSelected: Function;
    onDeselect: Function;
}

export class TimeseriesPlot extends Component<IFilterProps, {}> {

    shouldComponentUpdate(nextProps: IFilterProps): boolean {
        return !isEqual(nextProps.data, this.props.data);
    }

    render() {
        const {
            data,
            onSelected,
            onDeselect,
        } = this.props;

        const dateCounts = groupBy(data,
            d => roundDate(dayjs(d.ts), 'minute', 15));
        const tsPlot = barPlot(
            keys(dateCounts),
            values(dateCounts).map(group => group.length),
            'timeseries',
            {
                l: 20,
                r: 10,
                t: 30,
                b: 30,
            });

        return <Plot {...tsPlot}
                     divId='ts'
                     useResizeHandler={true}
                     onSelected={selected => onSelected(selected)}
                     onDeselect={() => onDeselect()}
                     style={plotStyle}/>;
    }
}
