import React, {Component} from 'react';
import Plot from 'react-plotly.js';
import {isEqual} from 'lodash';

import {countBarPlot, IEventData} from '../plot';
import {plotStyle} from '../Model';

interface IFilterProps {
    data: IEventData[];
    onSelected: Function;
    onDeselect: Function;
}

export class FilterPlots extends Component<IFilterProps, {}> {

    shouldComponentUpdate(nextProps: IFilterProps): boolean {
        return !isEqual(nextProps.data, this.props.data);
    }

    render() {
        const {
            data,
            onSelected,
            onDeselect,
        } = this.props;

        const filterPlots = ['reason', 'action', 'zone'].map(dimension => ({
            dimension,
            plot: countBarPlot(data, dimension),
        }));

        return filterPlots.map(plot =>
            <Plot {...plot.plot}
                  divId='reason'
                  useResizeHandler={true}
                  onSelected={selected => onSelected(plot.dimension, selected)}
                  onDeselect={() => onDeselect(plot.dimension)}
                  style={plotStyle}/>);
    }
}
