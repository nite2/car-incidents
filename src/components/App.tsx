import {Radio, Slider} from 'antd';
import {omit} from 'lodash';
// @ts-ignore
import kmeans from 'ml-kmeans';
import React, {Component} from 'react';
import {IAppState} from '../Model';
import {IEventData,} from '../plot';

import './App.scss';
import {FilterPlots} from './FilterPlots';
import {MapPlot} from './Map';
import {TimeseriesPlot} from './TimeseriesPlot';

const dataUrl = `http://localhost:8080/frontend_puzzle_data.json`;
const colorBy = 'zone';
const zoneCount = 8;
const defaultState = {
    data: [] as IEventData[],
    filters: {},
    timeRange: [],
    colorBy,
    zoneCount,
};

class App extends Component<{}, IAppState> {

    readonly state: IAppState;

    constructor(props: {}) {
        super(props);
        this.state = {...defaultState};
    }

    async componentDidMount() {
        const response = await fetch(dataUrl);
        let data = await response.json();
        data = this.kmeans(data, zoneCount);
        this.setState({data});
    }

    componentDidUpdate(prevProps: Readonly<{}>, prevState: Readonly<IAppState>): void {
        const {data, zoneCount} = this.state;
        if (prevState.zoneCount !== zoneCount) {
            this.setState({
                data: this.kmeans(data, zoneCount)
            });
        }
    }

    private kmeans(data: IEventData[], zoneCount: number) {
        const km = kmeans(data.map(d => [d.lat, d.long]), zoneCount);
        return data.map((d: any, i: number) => ({
            ...d,
            zone: km.clusters[i],
        }));
    }

    render() {
        const {
            data,
            colorBy,
            zoneCount,
        } = this.state;
        if (!data.length) {
            return <div/>;
        }

        const onSelected = (dimension: string, selection: any = null) => {
            const filters = selection && selection.points ? {
                ...this.state.filters,
                [dimension]: selection.points.map((p: any) => p.y),
            } : omit(this.state.filters, dimension);
            this.setState({filters});
        };

        const plainOptions = ['reason', 'action', 'zone'];

        return <div className='container'>
            <header>

                <div>
                    <label>Zone count: </label>
                    <Slider min={5}
                            max={15}
                            defaultValue={zoneCount}
                            style={{width: '200px'}}
                            onChange={(value) => {
                                const zoneCount = value as number;
                                return this.setState({zoneCount});
                            }}
                    />
                </div>

                <div>
                    <label>Colour by: </label>
                    <Radio.Group options={plainOptions}
                                 onChange={(e: any) => this.setState({colorBy: e.target.value})}
                                 value={colorBy}
                    />
                </div>

                <a href='#' onClick={() => {
                    this.setState({
                        ...defaultState,
                        data,
                    });
                }}>Reset</a>
            </header>
            <main className='columns'>
                <div className='column col-8'>
                    <MapPlot {...this.state} />
                </div>
                <div className='column col-4 container'>
                    <FilterPlots data={data}
                                 onDeselect={(dimension: string) => onSelected(dimension)}
                                 onSelected={onSelected}/>
                </div>
            </main>
            <footer>
                <TimeseriesPlot data={data}
                                onDeselect={(dimension: string) => {
                                    this.setState({timeRange: []});
                                }}
                                onSelected={(selection: any, dimension: string) => {
                                    let timeRange = selection.range.x.map((d: string) => new Date(d));
                                    this.setState({timeRange});
                                }}/>
            </footer>
        </div>;
    }
}

export default App;
